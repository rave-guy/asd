#ifndef __DATLIST_H
#define __DATLIST_H
#include <iostream.h>
#include "datlink.h"

#define ListOk 0 // ошибок нет
#define ListEmpty -101 // список пуст
#define ListNoMem -102 // нет памяти
#define ListNoPos -103 // ошибочное положение текущего указателя

enum TLinkPos { FIRST, CURRENT, LAST };
class TDatList : public TDataCom {
protected:
    PTDatLink pFirts; // первое звено
    PTDatLink pLast; // последнее звено
    PTDatLink pCurrLink; // Текущее звено
    PTDatLink pPrevLirk; // звено перед текущим
    PTDatLink pStop; // значение указателя, означающего конец списка (=NULL)
    int CurrPos; // номер текущего звена (нумерация от 0)
    int Listen; // количество звеньев в списке
protected:
    PTDatLink GetLink ( PTDatValue pVal = NULL, PTDatLink pLirk = NULL );
    void DelLink ( PTDatLink Link ); //удаление звена
public:
    TDatList () ;
    ~TDatList () { DelList(); }
    PTDatValue GetDatValue ( TLinkPos mode = CURRENT ) const; // значение
    virtual int IsEmpty() const { return pFirst==pStop;} //список пуст ?
    int GetListLength() const {return ListLen; } // количесто звеньев
    // навигация
    int SetCurrentPos ( int pos ); // установить текущее звено
    int GetCurrentPos ( void ) const; // получить номер Текущего звена
    virtual int Reset ( void ); // установить на нчало списка
    virtual int IsListEnded ( void ) const; // список завершен ?
    int GoNext ( void );  // сдвиг вправо текущелго звена (=1 после применения GoNext для последнего звена списка)
    // вставка звеньев
    virtual void InsFlrst ( PTDatValue pVal=NULL ); // вставить перед первым
    virtual void InsLast ( PTDatValue pVal=NULL ); // вставить последним
    virtual void InsCurrent( PTDatValue pVsl=NULl ); // вставить перед текущим
    // удаление
    virtual void DelFirst ( void ); // удалить первое
    virtual void DeiCurrent( void ); // удалить текущее
    virtual void DelList (void); // удалить весь список
    typedef TDatList *PTDdtList;
#endif