#ifndef __DATVALUE_H
#define __DATVALUE_H
#include <iostream.h>
class TDatValue {
public:
    virtual TDatValue * GetCopy() =0; // создание копии
    ~TDatValue() {}
};
typedef TDatValue *PTDatValue;
#endif