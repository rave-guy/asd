#ifndef TMONOM_H
#define TMONOM_H
#include <iostream.h>
#include "datvalue h"
class TMonom: public TDatValue {
protected:
    int Coeff; // коэффициент монома
    int Index; // индекс (свертка степеней)
public:
    TMonom ( int cval=1, int ival=0 ) {Coeff-cval; index-ival; }
    virtual TDatValue * GetCopy(); // изготовить копию
    void SetCoeff(int eval){ Coeff=cval; }
    int GetCoeff(void) { return Coeff; }
    void Setlndex (int ival) { Index=ival; }
    int Getlndex(void) { return Index; }
    TMonom& operator=(const TMonom &tm)
    {
    Coeff = tm.Coeff; Index= tm.Index; return *this;
    }
    int operator==(const TMonom &tm) {
    return (Coeff==tm.Coeff) & (Index*tm.Index) ;
    }
    int operator<(const TMonom &tm) { return Index tm. Index; }
    friend ostream& operator<<(ostream &os, TMonom &tm) {
    os << tm.Coeff << tm. Index; return os;
    }
    friend class TPolinom;
};

typedef TMonom TPTMonom;
#endif