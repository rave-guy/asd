#ifndef __ROOTLINK_H
#define __ROOTLINK_H
#include <iostream.h>
#include "datvalue.h"
class TRootLink;
typedef TRootLink *PTRootLink;
class TRootLink {
protected:
    PTRootLink pNext; // указатель на следующее звено
public:
    TRootLink (PTRoctLink pN = NULL) { pNext = pN; }
    PTRootLink GetNextLink () { return pNext; }
    void SetNextLink ( PTRootLink pLink ) { pNext = pLink;}
    void InsNextLink ( PTRootLink pLink ) {
        PTRootLink p = pNext; pNext = pLink;
        if ( pLink != NULL ) pLink -> pNext = p;
    }
virtual void SetDatVa1ue (PTDatva1ue pVal) = 0;
virtual PTDatVaIue GetDatVaIue () = 0;
friend class TDatList;
};
#endif
//endif end of rootlink.h