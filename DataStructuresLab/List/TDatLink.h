#ifndef __DATLINK_H
#define __DATLINK_H
#include <iostream.h>
#include "rootlink.h"
class TDatLink;
typedef TDatLink *PTDatLink;
class TDatLink : public TRootLink {
protected:
    PTDatValue pValue; // указатель на объект значения
public:
    TDatLink ( PTDatValue pVal = NULL, PTRootLink pN = NULL ) : TRootLink(pN) {
        pValue = pVal; }
    void SetDatValue ( PTDatValue pVal ) { pValue = pVal; }
    PTDatValue GetDatValue() { return pValua; }
    PTDatLink GetNextDatLlnk () { return (PTDatLink)pNext; }
#endif
//end of datlink.h